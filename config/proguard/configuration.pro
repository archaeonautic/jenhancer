-printmapping proguard.map

-libraryjars <java.home>/lib/rt.jar

-renamesourcefileattribute SourceFile
-keepattributes SourceFile,LineNumberTable

-keepattributes *Annotation*

-keepclasseswithmembers public class * {
    public static void main(java.lang.String[]);
}

-keepclasseswithmembernames,includedescriptorclasses class * {
    native <methods>;
}

-keepnames class de.archaeonautic.jenhancer.processitems.*, de.archaeonautic.jenhancer.processqueues.*

-keepclassmembers,allowoptimization enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

-keep class org.*{ *; }
-dontwarn org.**

-keep class nu.*{ *; }
-dontwarn nu.**

package de.archaeonautic.jenhancer.application;

import de.archaeonautic.jenhancer.enhance.ProcessBuffer;
import de.archaeonautic.jenhancer.processqueues.AbstractProcessQueue;
import de.archaeonautic.jenhancer.processqueues.LabAirLight;
import de.archaeonautic.jenhancer.processqueues.LabEnhancer;
import de.archaeonautic.jenhancer.processqueues.QueueProgressInfo;
import de.archaeonautic.util.ColorT;
import de.archaeonautic.util.PropertiesLoader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Main class for execution.
 */
public final class JEnhancer implements Observer {

    private static final Logger LOGGER = LoggerFactory.getLogger(JEnhancer.class);

    private static final String PROP_SECTION = "DIRECTORIES";
    private static final String DIR_INPUT = "dirInput";
    private static final String DIR_COLOR_REF = "dirColorRef";
    private static final String DIR_CHECK = "dirCheck";
    private static final String DIR_NOT_FOUND = "dirNotFound";
    private static final String DIR_FOUND = "dirFound";
    private static final String DIR_OUTPUT = "dirOutput";

    static {
        nu.pattern.OpenCV.loadShared();
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);
    }

    private static JEnhancer instance;
    private static ResourceBundle strings;

    private final ProcessBuffer procBuf;
    public List<AbstractProcessQueue> processQueues;

    private JEnhancer() {
        procBuf = new ProcessBuffer();
        processQueues = new ArrayList<>();
    }

    public static JEnhancer getInstance() {
        return instance == null ? instance = new JEnhancer() : instance;
    }

    public void init(final String iniPath) throws ExceptionInInitializerError {
        strings = ResourceBundle.getBundle("strings");

        try {
            if (!PropertiesLoader.initializied) {
                PropertiesLoader.init(iniPath);
            }
            this.initInputDir(PropertiesLoader.ini.get(PROP_SECTION, "INPUT"));
            this.initColorReferenceDir(PropertiesLoader.ini.get(PROP_SECTION, "COLORREFERENCE"));
            this.initOutputDir(PropertiesLoader.ini.get(PROP_SECTION, "OUTPUT"));
            this.procBuf.fileNamesIn = this.initFileList(this.procBuf.inputDir,
                    strings.getString(DIR_INPUT));
            this.procBuf.fileNamesColorRef = this.initFileList(this.procBuf.colorSourceDir,
                    strings.getString(DIR_COLOR_REF));
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw (ExceptionInInitializerError) new ExceptionInInitializerError(String.format(
                    strings.getString("initFailed"), JEnhancer.class.getName())).initCause(e);
        }
    }

    public void init(final String inputDir, final String colorReferenceDir, final String outputDir)
            throws ExceptionInInitializerError {
        this.initInputDir(inputDir);
        this.initColorReferenceDir(colorReferenceDir);
        this.initOutputDir(outputDir);
        this.procBuf.fileNamesIn = this.initFileList(this.procBuf.inputDir,
                strings.getString(DIR_INPUT));
        this.procBuf.fileNamesColorRef = this.initFileList(this.procBuf.colorSourceDir,
                strings.getString(DIR_COLOR_REF));
    }

    private void initInputDir(final String path) throws ExceptionInInitializerError {
        procBuf.inputDir = new File(path);
        LOGGER.info(String.format(strings.getString(DIR_CHECK), strings.getString(DIR_INPUT)));
        if (!procBuf.inputDir.exists()) {
            throw new ExceptionInInitializerError(String.format(strings.getString(DIR_NOT_FOUND),
                    strings.getString(DIR_INPUT), procBuf.inputDir));
        }
        LOGGER.info(String.format(strings.getString(DIR_FOUND), strings.getString(DIR_INPUT)));
    }

    private void initColorReferenceDir(final String path) throws ExceptionInInitializerError {
        procBuf.colorSourceDir = new File(path);
        LOGGER.info(String.format(strings.getString(DIR_CHECK), strings.getString(DIR_COLOR_REF)));
        if (!procBuf.colorSourceDir.exists()) {
            throw new ExceptionInInitializerError(String.format(strings.getString(DIR_NOT_FOUND),
                    strings.getString(DIR_COLOR_REF), procBuf.colorSourceDir));
        }
        LOGGER.info(String.format(strings.getString(DIR_FOUND), strings.getString(DIR_COLOR_REF)));
    }

    private void initOutputDir(final String path) throws ExceptionInInitializerError {
        procBuf.outputDir = new File(path);
        LOGGER.info(String.format(strings.getString(DIR_CHECK), strings.getString(DIR_OUTPUT)));
        if (procBuf.outputDir.exists()) {
            LOGGER.info(String.format(strings.getString(DIR_FOUND), strings.getString(DIR_OUTPUT)));
        } else {
            try {
                Files.createDirectories(Paths.get(path));
                LOGGER.info(String.format(strings.getString("dirCreated"),
                        strings.getString(DIR_OUTPUT)));
            } catch (IOException e) {
                throw (ExceptionInInitializerError) new ExceptionInInitializerError(
                        String.format(strings.getString(DIR_NOT_FOUND),
                                strings.getString(DIR_COLOR_REF),
                                procBuf.colorSourceDir)).initCause(e);
            }
        }
    }

    private List<String> initFileList(final File sourceDir, final String listName)
            throws ExceptionInInitializerError {
        LOGGER.info(String.format(strings.getString("imgSearch"), listName));

        final File[] files = sourceDir.listFiles();

        if (files == null) {
            throw new ExceptionInInitializerError(String.format(strings.getString("imgNotFound"),
                    listName));
        }

        final List<String> imgList = new ArrayList<>();
        int count = 0;

        for (final File file : sourceDir.listFiles()) {
            if (file.isFile() && JEnhancer.isAnImgFile(file)) {
                imgList.add(file.getName());
                count++;
                LOGGER.info(String.format(strings.getString("imgAdded"), file.getName()));
            } else {
                continue;
            }
        }

        if (count == 0) {
            throw new ExceptionInInitializerError(String.format(strings.getString("imgNotFound"),
                    listName));
        }

        LOGGER.info(String.format(strings.getString("imgFound"), count, listName));
        return imgList;
    }

    private static boolean isAnImgFile(final File file) {
        final String name = file.getName().toLowerCase();
        return name.endsWith(".jpg") || name.endsWith(".png");
    }

    public void greet() {
        // powered by: http://www.patorjk.com/software/taag/#p=display&f=Slant&t=jEnhancer
        System.out.println(ColorT.PURPLE);
        System.out.println("       _ ______      __");
        System.out.println("      (_) ____/___  / /_  ____ _____  ________  _____");
        System.out.println("     / / __/ / __ \\/ __ \\/ __ `/ __ \\/ ___/ _ \\/ ___/");
        System.out.print(ColorT.RED);
        System.out.println("    / / /___/ / / / / / / /_/ / / / / /__/  __/ /");
        System.out.print(ColorT.PURPLE);
        System.out.println(" __/ /_____/_/ /_/_/ /_/\\__,_/_/ /_/\\___/\\___/_/");
        System.out.println("/___/");
        System.out.println(ColorT.RESET);
        final Properties version = new Properties();

        try {
            version.load(this.getClass().getResourceAsStream("/version.properties"));
            System.out.println(String.format("%sVersion: %s%s\n\n",
                    ColorT.RED,
                    version.getProperty("VERSION_FULL"),
                    ColorT.RESET));
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public void run() {
        for (final AbstractProcessQueue pm : this.processQueues) {
            pm.run(this.procBuf);
        }
    }

    @Override
    public void update(final Observable observable, final Object arg) {
        final QueueProgressInfo progress = (QueueProgressInfo) arg;
        LOGGER.info(String.format(strings.getString("procProgress"),
                progress.getFinishedItems(),
                progress.getTotalItems(),
                strings.getString(observable.getClass().getSimpleName()))
        );
    }

    public static void main(final String... args) {
        if (args.length != 1) {
            System.out.println("Error: missing path to *.ini");
            System.out.println("usage -> java -jar JEnhancer.jar </path/to/config.ini>");
            return;
        }

        final JEnhancer jEnhancer = getInstance();
        jEnhancer.greet();
        jEnhancer.init(args[0]);

        final AbstractProcessQueue airLight = new LabAirLight();
        final AbstractProcessQueue enhance = new LabEnhancer();
        airLight.addObserver(jEnhancer);
        enhance.addObserver(jEnhancer);

        jEnhancer.processQueues.add(airLight);
        jEnhancer.processQueues.add(enhance);
        jEnhancer.run();
    }
}

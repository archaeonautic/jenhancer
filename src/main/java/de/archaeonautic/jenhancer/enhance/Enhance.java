package de.archaeonautic.jenhancer.enhance;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import java.util.List;

/**
 * Calculations for enhancement.
 */
public final class Enhance {

    private Enhance() {}

    public static void calcElemMax(final List<Mat> dim, final Mat res) {
        for (int d = dim.size() - 1; d > 1; d--) {
            Core.max(dim.get(d), dim.get(d - 2), dim.get(d - 2));
        }
        Core.max(dim.get(1), dim.get(0), res);
    }

    public static void calcElemSum(final List<Mat> dim, final Mat res) {
        for (int d = dim.size() - 1; d > 1; d--) {
            Core.add(dim.get(d), dim.get(d - 2), dim.get(d - 2));
        }
        Core.add(dim.get(1), dim.get(0), res);
    }
}

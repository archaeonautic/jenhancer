package de.archaeonautic.jenhancer.enhance;

import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides an image representation and its metadata.
 */
public final class ProcessBuffer {

    //compression parameters
    public final MatOfInt pngParams = new MatOfInt(Highgui.CV_IMWRITE_PNG_COMPRESSION, 0);
    public final MatOfInt jpgParams = new MatOfInt(Highgui.CV_IMWRITE_JPEG_QUALITY, 100);

    //list for splitting channels
    public List<Mat> channels;

    //Scalar holding airlight
    public Scalar airLight;

    //contains input/output Images dataformat
    public Mat inputImg;
    public Mat outputImg;

    //reference Image to working run
    public Mat reference;

    //holding single channel results
    public Mat singleResult;

    //holding multi channel results
    public Mat multiResult;

    //input/output folder
    public File inputDir;
    public File colorSourceDir;
    public File outputDir;

    //filenames
    public List<String> fileNamesIn;
    public List<String> fileNamesColorRef;
    public String currentFileName;

    public ProcessBuffer() {
        this.channels = new ArrayList<>();
        this.airLight = new Scalar(0, 0, 0);
        this.inputImg = new Mat();
        this.outputImg = new Mat();
        this.singleResult = new Mat();
        this.multiResult = new Mat();
        this.reference = new Mat();
        this.fileNamesIn = new ArrayList<>();
        this.fileNamesColorRef = new ArrayList<>();
    }

    public String getOutputName(final String filename) {
        return Paths.get(outputDir.toString(), filename).toString();
    }

    public String getInputName(final String filename) {
        return Paths.get(inputDir.toString(), filename).toString();
    }

    public String getColorSourceName(final String filename) {
        return Paths.get(colorSourceDir.toString(), filename).toString();
    }
}

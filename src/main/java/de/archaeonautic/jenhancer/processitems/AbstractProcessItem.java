package de.archaeonautic.jenhancer.processitems;

import java.util.Observable;

/**
 * Abstract Class for each process item.
 */
public abstract class AbstractProcessItem extends Observable implements Processable {

    public AbstractProcessItem() {
        super();
    }

    @Override
    public void notifyObservers() {
        notifyObservers(null);
    }

    @Override
    public void notifyObservers(final Object arg) {
        setChanged();
        super.notifyObservers(arg);
    }
}

package de.archaeonautic.jenhancer.processitems;

import de.archaeonautic.jenhancer.enhance.ProcessBuffer;

import org.opencv.core.Core;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.core.Scalar;

/**
 * Process item to estimate air light.
 */
public final class AirLightEstimation extends AbstractProcessItem {

    private Scalar currentClr;
    private final double threshold;
    private double mode;
    private double counter;

    public AirLightEstimation(final double threshold, final int mode) {
        super();
        this.threshold = threshold;
        this.mode = mode;
        this.counter = 1;
    }

    @Override
    public void run(ProcessBuffer buffer) {
        //get min position
        final MinMaxLocResult location = Core.minMaxLoc(buffer.singleResult);
        //verify if variance is beneath given threshold
        if (threshold > location.minVal || threshold <= 0.) {
            //get color from reference
            currentClr = new Scalar(buffer.reference.get(
                    (int) location.minLoc.y, (int) location.minLoc.x));

            //if counter > 1 the avg wil be calculated,
            //else airlight will be reset
            buffer.airLight.val[0] *= (this.counter - 1.) / this.counter;
            buffer.airLight.val[1] *= (this.counter - 1.) / this.counter;
            buffer.airLight.val[2] *= (this.counter - 1.) / this.counter;

            //get new airlight
            buffer.airLight.val[0] += (1. / this.counter) * this.currentClr.val[0];
            buffer.airLight.val[1] += (1. / this.counter) * this.currentClr.val[1];
            buffer.airLight.val[2] += (1. / this.counter) * this.currentClr.val[2];

            //increment counter by mode
            this.counter += this.mode;
        }

        notifyObservers(buffer.currentFileName);
    }
}

package de.archaeonautic.jenhancer.processitems;

import de.archaeonautic.jenhancer.enhance.ProcessBuffer;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

/**
 * Color space conversion  from RGB to Lab.
 */
public final class ConversionBGR2Lab extends AbstractProcessItem {

    private final Scalar scale;
    private final Scalar offset;

    public ConversionBGR2Lab() {
        super();
        this.scale = new Scalar(1. / 50., 1. / 127., 1. / 127.);
        this.offset = new Scalar(-1., 0., 0.);
    }

    @Override
    public void run(final ProcessBuffer buffer) {
        //convert input-image to normalized float
        buffer.inputImg.convertTo(buffer.reference, CvType.CV_32FC3, 1. / 255.);
        //convert to Lab-colorspace
        Imgproc.cvtColor(buffer.reference, buffer.reference, Imgproc.COLOR_BGR2Lab);
        //downscale to ensure radius = 1
        Core.multiply(buffer.reference, this.scale, buffer.reference);
        //shift sphere's center to origin
        Core.add(buffer.reference, this.offset, buffer.reference);

        notifyObservers(buffer.currentFileName);
    }
}

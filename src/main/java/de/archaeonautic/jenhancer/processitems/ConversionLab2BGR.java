package de.archaeonautic.jenhancer.processitems;

import de.archaeonautic.jenhancer.enhance.ProcessBuffer;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

/**
 * Color space conversion  from Lab to RGB.
 */
public final class ConversionLab2BGR extends AbstractProcessItem {

    private final Scalar scale;
    private final Scalar offset;

    public ConversionLab2BGR() {
        super();
        this.scale = new Scalar(50., 127., 127.);
        this.offset = new Scalar(1., 0., 0.);
    }

    @Override
    public void run(final ProcessBuffer buffer) {
        //reverting in-conversion
        //add offset
        Core.add(buffer.reference, this.offset, buffer.reference);
        //upscale
        Core.multiply(buffer.reference, this.scale, buffer.reference);
        //convert to RGB-colorspace
        Imgproc.cvtColor(buffer.reference, buffer.reference, Imgproc.COLOR_Lab2BGR);
        //convert to 8-bit unsigned
        buffer.reference.convertTo(buffer.outputImg, CvType.CV_8UC3, 255.);

        notifyObservers(buffer.currentFileName);
    }
}

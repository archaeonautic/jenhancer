package de.archaeonautic.jenhancer.processitems;

import de.archaeonautic.jenhancer.enhance.Enhance;
import de.archaeonautic.jenhancer.enhance.ProcessBuffer;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

/**
 * Process item to calculate morphological gradient pyramid.
 */
public final class MorphGradientPyramid extends AbstractProcessItem {

    private Mat patchKernel;
    private final List<Mat> pyramid;

    public MorphGradientPyramid() {
        super();
        this.patchKernel = new Mat();
        this.pyramid = new ArrayList<>();
    }

    @Override
    public void run(final ProcessBuffer buffer) {
        //estimate lower reference dimension
        int minDim = Math.min(buffer.reference.rows(), buffer.reference.cols());
        //estimate patchRadius
        final int patchRadius = minDim / 100;
        //create patchkernel
        patchKernel = Imgproc.getStructuringElement(
                Imgproc.MORPH_RECT, new Size(2 * patchRadius + 1, 2 * patchRadius + 1));
        //set scaleSize to reference size
        final Size scaleSize = new Size(buffer.reference.cols(), buffer.reference.rows());
        //initialize pyramid with reference image
        pyramid.add(buffer.reference.clone());

        //build pyramid
        for (minDim >>= 1; minDim > (3 * patchRadius); minDim >>= 1) {
            //half size
            scaleSize.width = ((int) scaleSize.width) >> 1;
            scaleSize.height = ((int) scaleSize.height) >> 1;
            //add new layer
            pyramid.add(new Mat());
            //downsample image
            Imgproc.pyrDown(pyramid.get(pyramid.size() - 2), pyramid.get(pyramid.size() - 1),
                    scaleSize);
        }

        //calculate pyramid
        for (int i = 0; i < pyramid.size(); i++) {
            //morphological gradient -> local patched max(I)-min(I)
            Imgproc.morphologyEx(pyramid.get(i), buffer.multiResult, Imgproc.MORPH_GRADIENT,
                    patchKernel);
            //max-recuction on channels
            Core.split(buffer.multiResult, buffer.channels);
            Enhance.calcElemMax(buffer.channels, pyramid.get(i));
        }

        //resolve pyramid
        for (int i = pyramid.size() - 1; i > 0; i--) {
            //upsample result
            Imgproc.resize(pyramid.get(i), buffer.singleResult, pyramid.get(i - 1).size(), 0, 0,
                    Imgproc.INTER_CUBIC);
            //add to layer above
            Core.add(pyramid.get(i - 1), buffer.singleResult, pyramid.get(i - 1));
        }

        //normalize to range 0 to 1
        //because of sphere's range from -1 to 1
        //multiply pyramidsize by 2
        Core.divide(pyramid.get(0), Scalar.all(2 * pyramid.size()), buffer.singleResult);
        //clean up pyramid
        pyramid.clear();

        notifyObservers(buffer.currentFileName);
    }
}

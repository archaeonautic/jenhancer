package de.archaeonautic.jenhancer.processitems;

import de.archaeonautic.jenhancer.enhance.ProcessBuffer;

/**
 * Interface which each process item should implement.
 */
public interface Processable {
    void run(ProcessBuffer buffer);
}

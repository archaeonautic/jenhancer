package de.archaeonautic.jenhancer.processitems;

import de.archaeonautic.jenhancer.enhance.ProcessBuffer;

import org.opencv.core.Core;
import org.opencv.core.Scalar;

/**
 * Process item to reconstruct colors.
 */
public final class Reconstruction extends AbstractProcessItem {

    public Reconstruction() {
        super();
    }

    @Override
    public void run(final ProcessBuffer buffer) {
        //combine transmission to 3-channel
        buffer.channels.clear();
        buffer.channels.add(buffer.singleResult.clone());
        buffer.channels.add(buffer.singleResult.clone());
        buffer.channels.add(buffer.singleResult.clone());
        Core.merge(buffer.channels, buffer.multiResult);
        //get airlight transmission
        Core.absdiff(buffer.multiResult, Scalar.all(1), buffer.multiResult);
        Core.multiply(buffer.multiResult, buffer.airLight, buffer.multiResult);
        //subtract airlight transmission
        Core.subtract(buffer.reference, buffer.multiResult, buffer.reference);
        //get direct transmission
        Core.merge(buffer.channels, buffer.multiResult);
        //get inverse
        Core.divide(1, buffer.multiResult, buffer.multiResult);
        //reduce transmission factor
        Core.pow(buffer.multiResult, 1. / 3., buffer.multiResult);
        //amplify signal
        Core.multiply(buffer.reference, buffer.multiResult, buffer.reference);

        notifyObservers(buffer.currentFileName);
    }
}

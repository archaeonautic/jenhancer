package de.archaeonautic.jenhancer.processitems;

import de.archaeonautic.jenhancer.enhance.Enhance;
import de.archaeonautic.jenhancer.enhance.ProcessBuffer;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;

/**
 * Process item to estimate transmission.
 */
public final class TransmissionEstimation extends AbstractProcessItem {

    private final Scalar a;
    private final Mat b;
    private final Mat c;

    public TransmissionEstimation() {
        super();
        a = new Scalar(0);
        b = new Mat();
        c = new Mat();
    }

    @Override
    public void run(final ProcessBuffer buffer) {
        /*
         * abc-formula
         * t = (b + sqrt(b^2+ac))/a
         * with
         * a = 1 - (A dot A)
         * b = (I dot A) - (A dot A)
         * c = (I - A) dot (I - A)
         */

        //a -> A dot A
        a.val[0] = Math.pow(buffer.airLight.val[0], 2) + Math.pow(buffer.airLight.val[1], 2)
                + Math.pow(buffer.airLight.val[2], 2);
        //b -> I dot A
        Core.multiply(buffer.reference, buffer.airLight, b);
        Core.split(b, buffer.channels);
        Enhance.calcElemSum(buffer.channels, b);
        //b -> b - a
        Core.subtract(b, a, b);
        //c -> I-A
        Core.subtract(buffer.reference, buffer.airLight, this.c);
        //c -> c dot c
        Core.pow(c, 2, c);
        Core.split(c, buffer.channels);
        Enhance.calcElemSum(buffer.channels, c);
        //a -> 1 - a
        a.val[0] = 1 - a.val[0];

        //ac
        Core.multiply(c, a, c);
        //b^2
        Core.pow(this.b, 2, buffer.singleResult);
        //b^2+ac
        Core.add(buffer.singleResult, this.c, buffer.singleResult);
        //sqrt(b^2+ac)
        Core.sqrt(buffer.singleResult, buffer.singleResult);
        //b+sqrt(b^2+ac)
        Core.add(b, buffer.singleResult, buffer.singleResult);
        //(b+sqrt(b^2+ac))/a
        Core.divide(buffer.singleResult, this.a, buffer.singleResult);

        notifyObservers(buffer.currentFileName);
    }
}

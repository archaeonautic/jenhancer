package de.archaeonautic.jenhancer.processitems;

import de.archaeonautic.jenhancer.enhance.ProcessBuffer;

import org.opencv.core.Core;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

/**
 * Process item to refine transmission.
 */
public final class TransmissionRefinement extends AbstractProcessItem {

    private final double limiter;
    private final Scalar contrast;

    public TransmissionRefinement(final double limit) {
        super();
        this.limiter = limit;
        this.contrast = new Scalar(1. / 256.);
    }

    @Override
    public void run(final ProcessBuffer buffer) {
        //verifiy upper boundary
        Imgproc.threshold(buffer.singleResult, buffer.singleResult, 1, 0, Imgproc.THRESH_TRUNC);
        Core.subtract(buffer.singleResult, this.contrast, buffer.singleResult);
        //verify lower boundary
        Imgproc.threshold(buffer.singleResult, buffer.singleResult, this.contrast.val[0], 0,
                Imgproc.THRESH_TOZERO);
        Core.add(buffer.singleResult, this.contrast, buffer.singleResult);
        //get relative depth
        //Core.log(pB.singleResult, pB.singleResult);
        //Core.divide(pB.singleResult, expContrast, pB.singleResult);
        //scale depth towards eyepoint
        //Core.multiply(pB.singleResult, Scalar.all(this.limiter), pB.singleResult);
        //calculate back
        //Core.exp(pB.singleResult, pB.singleResult);
        Core.pow(buffer.singleResult, this.limiter, buffer.singleResult);

        notifyObservers(buffer.currentFileName);
    }
}

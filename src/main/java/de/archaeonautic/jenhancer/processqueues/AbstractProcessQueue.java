package de.archaeonautic.jenhancer.processqueues;

import de.archaeonautic.jenhancer.processitems.AbstractProcessItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.ResourceBundle;

/**
 * Abstract class from which all process queues should inherit.
 */
public abstract class AbstractProcessQueue extends AbstractProcessItem implements Observer {

    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractProcessQueue.class);

    protected ResourceBundle strings;

    protected List<AbstractProcessItem> processItems;
    protected QueueProgressInfo progress;

    protected AbstractProcessQueue() {
        super();
        strings = ResourceBundle.getBundle("strings");
        processItems = new ArrayList<>();
        progress = new QueueProgressInfo(0, 0);
    }


    @Override
    public void update(final Observable observable, final Object arg) {
        progress.incrementFinishedItems();
        LOGGER.info(String.format(
                strings.getString("procFinished"),
                arg,
                strings.getString(observable.getClass().getSimpleName()))
        );
        notifyObservers(progress);
    }
}

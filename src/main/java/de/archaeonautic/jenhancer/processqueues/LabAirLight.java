package de.archaeonautic.jenhancer.processqueues;

import de.archaeonautic.jenhancer.enhance.ProcessBuffer;
import de.archaeonautic.jenhancer.processitems.AirLightEstimation;
import de.archaeonautic.jenhancer.processitems.ConversionBGR2Lab;
import de.archaeonautic.jenhancer.processitems.MorphGradientPyramid;
import de.archaeonautic.jenhancer.processitems.AbstractProcessItem;

import org.opencv.highgui.Highgui;

/**
 * Queue of process items to calculate air light for a given set of images.
 */
public final class LabAirLight extends AbstractProcessQueue {

    public LabAirLight() {
        super();
        final AbstractProcessItem convert = new ConversionBGR2Lab();
        final AbstractProcessItem morph = new MorphGradientPyramid();
        final AbstractProcessItem airLight = new AirLightEstimation(0.06, 1);
        convert.addObserver(this);
        morph.addObserver(this);
        airLight.addObserver(this);
        processItems.add(convert);
        processItems.add(morph);
        processItems.add(airLight);
    }

    @Override
    public void run(final ProcessBuffer buffer) {
        progress.setTotalItems(buffer.fileNamesColorRef.size() * processItems.size());
        for (int i = 0; i < buffer.fileNamesColorRef.size(); i++) {
            buffer.currentFileName = buffer.fileNamesColorRef.get(i);
            buffer.inputImg = Highgui.imread(buffer.getColorSourceName(buffer.currentFileName));
            if (buffer.inputImg.empty()) {
                continue;
            }
            for (final AbstractProcessItem item : processItems) {
                item.run(buffer);
            }
        }
    }
}

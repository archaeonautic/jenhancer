package de.archaeonautic.jenhancer.processqueues;

import de.archaeonautic.jenhancer.enhance.ProcessBuffer;
import de.archaeonautic.jenhancer.processitems.AbstractProcessItem;
import de.archaeonautic.jenhancer.processitems.ConversionBGR2Lab;
import de.archaeonautic.jenhancer.processitems.ConversionLab2BGR;
import de.archaeonautic.jenhancer.processitems.Reconstruction;
import de.archaeonautic.jenhancer.processitems.TransmissionEstimation;
import de.archaeonautic.jenhancer.processitems.TransmissionRefinement;
import org.opencv.highgui.Highgui;

/**
 * Queue of process items to enhance a given set of images.
 */
public final class LabEnhancer extends AbstractProcessQueue {

    public LabEnhancer() {
        super();
        final AbstractProcessItem convertBGR = new ConversionBGR2Lab();
        final AbstractProcessItem transmEst = new TransmissionEstimation();
        final AbstractProcessItem transmRef = new TransmissionRefinement(0.95);
        final AbstractProcessItem reconstr = new Reconstruction();
        final AbstractProcessItem convertLab = new ConversionLab2BGR();
        convertBGR.addObserver(this);
        transmEst.addObserver(this);
        transmRef.addObserver(this);
        reconstr.addObserver(this);
        convertLab.addObserver(this);
        processItems.add(convertBGR);
        processItems.add(transmEst);
        processItems.add(transmRef);
        processItems.add(reconstr);
        processItems.add(convertLab);
    }

    @Override
    public void run(final ProcessBuffer buffer) {
        progress.setTotalItems(buffer.fileNamesIn.size() * processItems.size());
        for (int i = 0; i < buffer.fileNamesIn.size(); i++) {
            buffer.currentFileName = buffer.fileNamesIn.get(i);
            buffer.inputImg = Highgui.imread(buffer.getInputName(buffer.currentFileName));
            if (buffer.inputImg.empty()) {
                continue;
            }
            for (final AbstractProcessItem item : processItems) {
                item.run(buffer);
            }
            final String fName = buffer.getOutputName(buffer.fileNamesIn.get(i));
            if (fName.endsWith(".jpg") || fName.endsWith(".JPG")) {
                Highgui.imwrite(fName, buffer.outputImg, buffer.jpgParams);
            } else if (fName.endsWith(".png") || fName.endsWith(".PNG")) {
                Highgui.imwrite(fName, buffer.outputImg, buffer.pngParams);
            }
        }
    }
}

package de.archaeonautic.jenhancer.processqueues;

/**
 * Provides information about the queue's progress.
 */
public final class QueueProgressInfo {
    private int totalItems;
    private int finishedItems;
    private float progress;
    private float progressPercentage;

    public QueueProgressInfo(final int totalItems, final int finishedItems) {
        this.totalItems = totalItems;
        this.finishedItems = finishedItems;
        updateProgress();
    }

    public int getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(final int totalItems) {
        this.totalItems = totalItems;
        updateProgress();
    }

    public int getFinishedItems() {
        return finishedItems;
    }

    public void incrementFinishedItems() {
        finishedItems++;
        updateProgress();
    }

    public void decrementFinishedItems() {
        finishedItems--;
        updateProgress();
    }

    public float getProgress() {
        return progress;
    }

    public float getProgressPercentage() {
        return progressPercentage;
    }

    private void updateProgress() {
        progress = (float) finishedItems / totalItems;
        progressPercentage = progress * 100.0f;
    }
}

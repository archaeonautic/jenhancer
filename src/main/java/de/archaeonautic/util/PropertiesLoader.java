package de.archaeonautic.util;

import org.ini4j.Config;
import org.ini4j.Ini;
import java.io.FileReader;
import java.io.IOException;

/**
 * Wrapper for properties files - such as *.ini.
 */
public final class PropertiesLoader {
    public static Ini ini;
    public static boolean initializied;

    private PropertiesLoader() {
        Config.getGlobal().setEscape(false);
        initializied = false;
    }

    public static void init(final String filename) throws IOException {
        ini = new Ini(new FileReader(filename));
        initializied = true;
    }
}

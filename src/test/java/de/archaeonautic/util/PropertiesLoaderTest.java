package de.archaeonautic.util;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Test parametrisation with ini-files.
 */
public class PropertiesLoaderTest {

    @Test(expected = IOException.class)
    public void initException() throws IOException {
        PropertiesLoader.init("");
    }

    @Test
    public void init() throws Exception {
        final String section = "DIRECTORIES";
        PropertiesLoader.init(this.getClass().getResource("/properties.ini").getPath());

        assertTrue(PropertiesLoader.initializied);
        assertNotNull(PropertiesLoader.ini);
        assertNotNull(PropertiesLoader.ini.get(section));
        assertEquals("./build/resources/test/img/Veruda",
                PropertiesLoader.ini.fetch(section, "INPUT"));
    }
}

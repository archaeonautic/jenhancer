package de.archaeonautic.util;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Test suite of util package.
 */
@RunWith(Suite.class) @Suite.SuiteClasses({
        PropertiesLoaderTest.class
        })
public class UtilTest {}
